FROM python:3.7.2

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1


WORKDIR /blog
COPY . .
RUN pip install --upgrade pip && pip install -r requirements.txt