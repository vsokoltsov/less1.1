## Provision resources

* `cd deploy`
* Copy service account json key to `deploy` folder with `serviceaccount` name
* `terraform init`
* `terraform apply`


## Provision application
* `cd deploy`
* `ansible-playbook -u ansible -i ./hosts --private-key=./ansible playbook.yaml --ssh-common-args='-o StrictHostKeyChecking=no'`