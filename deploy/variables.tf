variable "ssh_username" {
    type = string
}

variable "ssh_pub_key_path" {
    type = string
}

variable "ssh_private_key_path" {
    type = string
}

provider "google" {
    credentials = file("serviceaccount.json")
    project     = "blog-276519"
    region      = "us-central1"
}